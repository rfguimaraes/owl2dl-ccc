# OWL2DL - Change Case Creator

Applies the changes specified in a JSON file to the ontology and outputs the
catalysts of behaviour. 

It is intended to modify ontologies producing interesting test cases for
Belief Change in Description Logic Bases, test reasoners and other
applications.

## Building

We use Gradle to build the application:

1. Set up Gradle: https://gradle.org/install/ (we strongly recommend setting up gradle daemon too).
2. Clone this repository.
3. Open the root directory and type: ```./gradlew check```.
4. If everything is Ok, type: ```./gradlew build```. This should build the application and run the unit tests.
    
    - You can find the resulting files in the `build` directory.

5. You can also generate a distributable tar and zip files using the tasks `./gradlew distTar` and `./gradlew distZip`, respectively.

    - You can find the resulting files in the `build/distributions` directory.
    - After extracting them, you will find wrapper scripts in the `bin`directory.
    - You also find examples in the `examples`directory.

## Using Docker

We also provide a dockerised version of OWL2DL-CCC.
To build it you can use the command:
```
docker build -t owl2dl-ccc .
```

If the build fails due to some dependency not being found, we recommend trying the following command instead.
```
docker build --network=host -t owl2dl-ccc .
```

Then, the container can be run as an application, for example:

```
sudo docker run --rm -it owl2dl-ccc --help
```


Files can be passed to the container as usual.
For example, one can use volumes with a command similar to the one below.
```
docker run --rm -it -v /path/to/input/files:/data -v /path/to/output/files:/results owl2dl-ccc -c /data/specs/fullExample.json -d /results -r HermiT -g 10 -s /data/ontologies/alpha_full.owl.xml
```

## Help

The binary generated has a "help" option which displays the arguments
accepted. Furthermore, there the shell script in `src/dist/bin/casegen.sh`
illustrates how to call the program after building. 
The distribution also includes ontologies and specification files as examples.

A [manual](https://gitlab.com/rfguimaraes/owl2dl-ccc/-/blob/master/src/dist/owl2dl-ccc-manual.pdf) with additional help and descriptions may be found in the `src/dist` directory.
