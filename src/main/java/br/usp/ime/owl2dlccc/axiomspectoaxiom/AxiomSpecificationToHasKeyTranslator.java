/*
 *    Copyright 2018-2019,2023 OWL 2 DL - Change Case Creator Developers
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package br.usp.ime.owl2dlccc.axiomspectoaxiom;

import br.usp.ime.owl2dlccc.Match;
import br.usp.ime.owl2dlccc.model.AxiomSpecification;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import org.semanticweb.owlapi.model.OWLClassExpression;
import org.semanticweb.owlapi.model.OWLHasKeyAxiom;
import org.semanticweb.owlapi.model.OWLPropertyExpression;
import uk.ac.manchester.cs.owl.owlapi.OWLHasKeyAxiomImpl;

class AxiomSpecificationToHasKeyTranslator extends
    AxiomSpecificationToAxiomTranslator<OWLHasKeyAxiom> {

  @Override
  Optional<OWLHasKeyAxiom> translateInternal(AxiomSpecification axiomSpecification,
      Match match) {
    List<String> arguments = axiomSpecification.getArguments();
    Optional<OWLClassExpression> optClassExpression = match.getClassExpression(arguments.get(0));
    List<OWLPropertyExpression> properties = arguments.subList(1, arguments.size()).stream()
        .map(s -> getPropertyFromAttribution(s, match))
        .flatMap(o -> o.map(Stream::of).orElseGet(Stream::empty))
        .collect(Collectors.toList());

    return Optional.ofNullable((optClassExpression.isPresent() && !properties.isEmpty()) ?
        new OWLHasKeyAxiomImpl(optClassExpression.get(), properties, Collections.emptySet())
        : null);
  }

  private Optional<? extends OWLPropertyExpression> getPropertyFromAttribution(String key,
      Match match) {
    Optional<? extends OWLPropertyExpression> optProperty = match
        .getObjectPropertyExpression(key);
    if (optProperty.isPresent()) {
      return optProperty;
    }
    return match.getDataPropertyExpression(key);
  }
}
