/*
 *    Copyright 2018-2019,2023 OWL 2 DL - Change Case Creator Developers
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package br.usp.ime.owl2dlccc.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.Optional;

public interface PrimitiveTerm extends Term {

  @JsonProperty("used")
  Optional<Boolean> getUsed();

  @JsonProperty("used")
  void setUsed(Boolean used);

  @JsonProperty("complex")
  Optional<Boolean> getComplex();

  @JsonProperty("complex")
  void setComplex(Boolean complex);

  @JsonProperty("owl_thing")
  Optional<Boolean> getOWLThing();

  @JsonProperty("owl_thing")
  void setOWLThing(Boolean isOWLThing);

  @JsonProperty("owl_nothing")
  Optional<Boolean> getOWLNothing();

  @JsonProperty("owl_nothing")
  void setOWLNothing(Boolean isOWLNothing);
}
