/*
 *    Copyright 2018-2019,2023 OWL 2 DL - Change Case Creator Developers
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package br.usp.ime.owl2dlccc.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.List;
import java.util.Optional;

public class ActionBlockImpl implements ActionBlock {

  protected List<AxiomSpecification> additions;
  protected List<AxiomSpecification> removals;

  @JsonCreator
  public ActionBlockImpl(
      @JsonProperty(value = "add") List<AxiomSpecification> additions,
      @JsonProperty(value = "remove") List<AxiomSpecification> removals
  ) {
    this.additions = additions;
    this.removals = removals;
  }

  @Override
  public Optional<List<AxiomSpecification>> getAdditions() {
    return Optional.ofNullable(additions);
  }

  @Override
  public void setAdditions(List<AxiomSpecification> additions) {
    this.additions = additions;
  }

  @Override
  public Optional<List<AxiomSpecification>> getRemovals() {
    return Optional.ofNullable(removals);
  }

  @Override
  public void setRemovals(List<AxiomSpecification> removals) {
    this.removals = removals;
  }
}
