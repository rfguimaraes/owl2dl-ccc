/*
 *    Copyright 2018-2019,2023 OWL 2 DL - Change Case Creator Developers
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package br.usp.ime.owl2dlccc.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.List;
import java.util.Optional;

public class IntegrityConstraintsImpl implements IntegrityConstraints {

  protected Boolean enforceCoherence;
  protected Boolean enforceConsistency;
  protected List<AxiomRequirement> axiomRequirements;

  @JsonCreator
  public IntegrityConstraintsImpl(
      @JsonProperty(value = "coherence") Boolean enforceCoherence,
      @JsonProperty(value = "consistency") Boolean enforceConsistency,
      @JsonProperty(value = "requirements") List<AxiomRequirement> axiomRequirements) {
    this.enforceCoherence = enforceCoherence;
    this.enforceConsistency = enforceConsistency;
    this.axiomRequirements = axiomRequirements;
  }

  @Override
  public Optional<Boolean> enforceCoherence() {
    return Optional.ofNullable(enforceCoherence);
  }

  @Override
  public void setCoherenceEnforcement(Boolean coherenceEnforcement) {
    this.enforceCoherence = coherenceEnforcement;
  }

  @Override
  public Optional<Boolean> enforceConsistency() {
    return Optional.ofNullable(enforceConsistency);
  }

  @Override
  public void setConsistencyEnforcement(Boolean consistencyEnforcement) {
    this.enforceConsistency = consistencyEnforcement;
  }

  @Override
  public Optional<List<AxiomRequirement>> getAxiomRequirements() {
    return Optional.ofNullable(axiomRequirements);
  }

  public void setAxiomRequirements(
      List<AxiomRequirement> axiomRequirements) {
    this.axiomRequirements = axiomRequirements;
  }
}
