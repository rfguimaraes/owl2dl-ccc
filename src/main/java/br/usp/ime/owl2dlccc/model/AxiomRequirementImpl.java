/*
 *    Copyright 2018-2019,2023 OWL 2 DL - Change Case Creator Developers
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package br.usp.ime.owl2dlccc.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.Optional;

public class AxiomRequirementImpl implements AxiomRequirement {

  @JsonProperty(value = "axiom")
  protected AxiomSpecification axiomSpecification;
  protected Boolean isAsserted;
  protected Boolean isEntailed;

  @JsonCreator
  public AxiomRequirementImpl(
      @JsonProperty(value = "axiom", required = true) AxiomSpecification axiomSpecification,
      @JsonProperty(value = "asserted") Boolean isAsserted,
      @JsonProperty(value = "entailed") Boolean isEntailed) {
    this.axiomSpecification = axiomSpecification;
    this.isAsserted = isAsserted;
    this.isEntailed = isEntailed;
  }

  @Override
  public AxiomSpecification getAxiomSpecification() {
    return axiomSpecification;
  }

  @Override
  public void setAxiomSpecification(AxiomSpecification axiomSpecification) {
    this.axiomSpecification = axiomSpecification;
  }

  @Override
  public Optional<Boolean> isAsserted() {
    return Optional.ofNullable(isAsserted);
  }

  @Override
  public void setAsserted(Boolean isAsserted) {
    this.isAsserted = isAsserted;
  }

  @Override
  public Optional<Boolean> isEntailed() {
    return Optional.ofNullable(isEntailed);
  }

  @Override
  public void setEntailed(Boolean isEntailed) {
    this.isEntailed = isEntailed;
  }
}
