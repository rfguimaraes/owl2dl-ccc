/*
 *    Copyright 2018-2020,2023 OWL 2 DL - Change Case Creator Developers
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package br.usp.ime.owl2dlccc.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

public class ChangeSpecificationImpl implements ChangeSpecification {

  protected HeaderBlock headerBlock;
  protected TermsBlock termsBlock;
  protected IntegrityConstraints preconditions;
  protected ActionBlock actionBlock;
  protected IntegrityConstraints postconditions;
  protected List<CatalystBlock> catalysts;

  @JsonCreator
  public ChangeSpecificationImpl(
      @JsonProperty(value = "header", required = true) HeaderBlock headerBlock,
      @JsonProperty(value = "terms") TermsBlock termsBlock,
      @JsonProperty(value = "preconditions") IntegrityConstraints preconditions,
      @JsonProperty(value = "actions") ActionBlock actionBlock,
      @JsonProperty(value = "postconditions") IntegrityConstraints postconditions,
      @JsonProperty(value = "catalysts") List<CatalystBlock> catalysts) {
    this.headerBlock = headerBlock;
    this.termsBlock = termsBlock;
    this.preconditions = preconditions;
    this.actionBlock = actionBlock;
    this.postconditions = postconditions;
    this.catalysts = catalysts;
  }

  @Override
  public HeaderBlock getHeaderBlock() {
    return headerBlock;
  }

  @Override
  public void setHeaderBlock(HeaderBlock headerBlock) {
    this.headerBlock = headerBlock;
  }

  @Override
  public Optional<TermsBlock> getTermsBlock() {
    return Optional.ofNullable(termsBlock);
  }

  @Override
  public void setTermsBlock(TermsBlock termsBlock) {
    this.termsBlock = termsBlock;
  }

  @Override
  public Optional<IntegrityConstraints> getPreconditions() {
    return Optional.ofNullable(preconditions);
  }

  @Override
  public void setPreconditions(IntegrityConstraints preconditions) {
    this.preconditions = preconditions;
  }

  @Override
  public Optional<ActionBlock> getActionBlock() {
    return Optional.ofNullable(actionBlock);
  }

  @Override
  public void setActionBlock(ActionBlock actionBlock) {
    this.actionBlock = actionBlock;
  }

  @Override
  public Optional<IntegrityConstraints> getPostconditions() {
    return Optional.ofNullable(this.postconditions);
  }

  @Override
  public void setPostconditions(IntegrityConstraints postconditions) {
    this.postconditions = postconditions;
  }

  @Override
  public List<CatalystBlock> getCatalysts() {
    return (catalysts == null) ? Collections.emptyList() : catalysts;
  }

  @Override
  public void setCatalysts(List<CatalystBlock> catalysts) {
    this.catalysts = catalysts;
  }
}
