/*
 *    Copyright 2018-2019,2023 OWL 2 DL - Change Case Creator Developers
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package br.usp.ime.owl2dlccc.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.List;
import org.semanticweb.owlapi.model.AxiomType;

public class AxiomSpecificationImpl implements AxiomSpecification {

  protected AxiomType axiomType;
  protected List<String> arguments;

  @JsonCreator
  public AxiomSpecificationImpl(
      @JsonProperty(value = "axiom_type", required = true) AxiomType axiomType,
      @JsonProperty(value = "arguments", required = true) List<String> arguments) {
    this.axiomType = axiomType;
    this.arguments = arguments;
  }

  @Override
  public AxiomType getAxiomType() {
    return axiomType;
  }

  @Override
  public void setAxiomType(AxiomType axiomType) {
    this.axiomType = axiomType;
  }

  @Override
  public List<String> getArguments() {
    return arguments;
  }

  @Override
  public void setArguments(List<String> arguments) {
    this.arguments = arguments;
  }
}
