/*
 *    Copyright 2018-2019,2023 OWL 2 DL - Change Case Creator Developers
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package br.usp.ime.owl2dlccc;

import br.usp.ime.owl2dlccc.axiomspectoaxiom.AxiomSpecificationToOWLAxiomTranslator;
import br.usp.ime.owl2dlccc.model.ActionBlock;
import java.util.stream.Stream;
import org.semanticweb.owlapi.model.OWLLogicalAxiom;
import org.semanticweb.owlapi.model.OWLOntology;
import org.semanticweb.owlapi.model.OWLOntologyCreationException;
import org.semanticweb.owlapi.model.parameters.ChangeApplied;

public class ActionManager {

  private final OWLOntology ontology;
  private final Match match;
  private final ActionBlock actionBlock;

  public ActionManager(ActionBlock actionBlock, OWLOntology ontology, Match match) {

    this.actionBlock = actionBlock;
    this.ontology = ontology;
    this.match = match;
  }

  public Stream<OWLLogicalAxiom> getAdditions() {
    if (actionBlock.getAdditions().isPresent()) {
      return actionBlock.getAdditions().get().stream().map(
          axiomSpecification -> AxiomSpecificationToOWLAxiomTranslator
              .translate(axiomSpecification, match)).flatMap(
          o -> o.map(Stream::of).orElseThrow(() -> new RuntimeException("Failed to build action")));
    }
    return Stream.empty();
  }

  public Stream<OWLLogicalAxiom> getRemovals() {
    if (actionBlock.getRemovals().isPresent()) {
      return actionBlock.getRemovals().get().stream().map(
          axiomSpecification -> AxiomSpecificationToOWLAxiomTranslator
              .translate(axiomSpecification, match)).flatMap(
          o -> o.map(Stream::of).orElseThrow(() -> new RuntimeException("Failed to build action")));
    }
    return Stream.empty();
  }

  public OWLOntology applyChanges()
      throws OWLOntologyCreationException, SpecificationValidationException {
    OWLOntology changedOntology = ontology.getOWLOntologyManager()
        .createOntology(ontology.axioms());

    ChangeApplied changeApplied = changedOntology.removeAxioms(this.getRemovals());
    if (changeApplied == ChangeApplied.UNSUCCESSFULLY) {
      throw new SpecificationValidationException("Could not remove axioms.");
    }

    changeApplied = changedOntology.addAxioms(this.getAdditions());
    if (changeApplied == ChangeApplied.UNSUCCESSFULLY) {
      throw new SpecificationValidationException("Could not add axioms.");
    }

    return changedOntology;
  }
}
