/*
 *    Copyright 2018-2019,2023 OWL 2 DL - Change Case Creator Developers
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package br.usp.ime.owl2dlccc.parsing;

import java.io.IOException;
import java.io.Reader;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.Module;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.PropertyNamingStrategies;
import com.fasterxml.jackson.datatype.jdk8.Jdk8Module;

import br.usp.ime.owl2dlccc.model.ChangeSpecification;

public class ChangeSpecificationParser {

  public static ChangeSpecification parseChangeSpecification(String changeJSON, Module typeModule)
      throws IOException {

    ObjectMapper objectMapper = new ObjectMapper();
    objectMapper.registerModule(new Jdk8Module());
    objectMapper.registerModule(typeModule);
    objectMapper.setPropertyNamingStrategy(PropertyNamingStrategies.SNAKE_CASE);
    objectMapper.configure(DeserializationFeature.FAIL_ON_NULL_FOR_PRIMITIVES, true);

    return objectMapper
        .readValue(changeJSON, ChangeSpecification.class);
  }

  public static ChangeSpecification parseChangeSpecification(String changeJSON)
      throws IOException {

    return DefaultObjectMapper.getInstance()
        .readValue(changeJSON, ChangeSpecification.class);
  }

  public static ChangeSpecification parseChangeSpecification(Reader reader)
      throws IOException {

    return DefaultObjectMapper.getInstance()
        .readValue(reader, ChangeSpecification.class);
  }

}
