/*
 *    Copyright 2018-2020,2023 OWL 2 DL - Change Case Creator Developers
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package br.usp.ime.owl2dlccc;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jdk8.Jdk8Module;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;
import org.semanticweb.owlapi.apibinding.OWLManager;
import org.semanticweb.owlapi.formats.OWLXMLDocumentFormat;
import org.semanticweb.owlapi.io.OWLOntologyDocumentSource;
import org.semanticweb.owlapi.io.OWLOntologyDocumentTarget;
import org.semanticweb.owlapi.io.ReaderDocumentSource;
import org.semanticweb.owlapi.io.WriterDocumentTarget;
import org.semanticweb.owlapi.model.OWLAxiom;
import org.semanticweb.owlapi.model.OWLLogicalAxiom;
import org.semanticweb.owlapi.model.OWLOntology;
import org.semanticweb.owlapi.model.OWLOntologyCreationException;
import org.semanticweb.owlapi.model.OWLOntologyManager;
import org.semanticweb.owlapi.model.OWLOntologyStorageException;

public class CaseInstance {

  private final String name;
  private final String type;
  private final Boolean enforceCoherence;
  private final Boolean enforceConsistency;
  private final Set<OWLLogicalAxiom> positives;
  private final Set<OWLLogicalAxiom> negatives;
  private final OWLOntology modifiedOntology;
  private final Path sourceOntologyPath;
  private final String caseName;
  private final Path outputDir;
  private final ObjectMapper mapper = new ObjectMapper().enable(SerializationFeature.INDENT_OUTPUT)
      .registerModule(new Jdk8Module());
  private final OWLOntologyManager manager;
  private final int multiplicity;
  private final int iteration;
  private final String version;
  private final long seed;

  public CaseInstance(String name, String type,
      Boolean enforceCoherence, Boolean enforceConsistency,
      Set<OWLLogicalAxiom> positives,
      Set<OWLLogicalAxiom> negatives,
      OWLOntology sourceOntology, Path sourceOntologyPath,
      String caseName, Path outputDir, int multiplicity, int iteration, String version, long seed) {
    this.name = name;
    this.type = type;
    this.enforceCoherence = enforceCoherence;
    this.enforceConsistency = enforceConsistency;
    this.positives = positives;
    this.negatives = negatives;
    this.modifiedOntology = sourceOntology;
    this.sourceOntologyPath = sourceOntologyPath;
    this.caseName = caseName;
    this.outputDir = outputDir;
    this.multiplicity = multiplicity;
    this.iteration = iteration;
    this.version = version;
    this.seed = seed;
    this.manager = OWLManager.createOWLOntologyManager();

  }

  public CaseInstance(Path path, OWLOntologyManager manager)
      throws IOException, OWLOntologyCreationException {

    try (BufferedReader reader = Files.newBufferedReader(path)) {
      CaseData caseData = mapper.readValue(reader, CaseData.class);
      this.name = caseData.name;
      this.type = caseData.type;
      this.enforceCoherence = caseData.enforceCoherence;
      this.enforceConsistency = caseData.enforceConsistency;
      this.sourceOntologyPath = caseData.sourceOntologyPath;
      this.caseName = caseData.caseName;
      this.outputDir = caseData.outputDir;
      this.multiplicity = caseData.multiplicity;
      this.version = caseData.version;
      this.seed = caseData.seed;
      this.iteration = caseData.iteration;
      this.manager = manager;
      String baseName = name + "_" + type + "_" + iteration;
      this.modifiedOntology = loadModifiedOntology(caseData.getTargetOntologyPath());
      this.positives = loadAxioms(Paths.get(outputDir.toString(), baseName + "_pos.owl.xml"));
      this.negatives = loadAxioms(Paths.get(outputDir.toString(), baseName + "_neg.owl.xml"));
    }
  }

  private OWLOntology loadModifiedOntology(Path ontologyPath)
      throws IOException, OWLOntologyCreationException {
    try (BufferedReader reader = Files.newBufferedReader(ontologyPath)) {
      OWLOntologyDocumentSource documentSource = new ReaderDocumentSource(reader);
      return manager.loadOntologyFromOntologyDocument(documentSource);
    }
  }

  private Set<OWLLogicalAxiom> loadAxioms(Path path)
      throws IOException, OWLOntologyCreationException {
    try (BufferedReader reader = Files.newBufferedReader(path)) {
      OWLOntologyDocumentSource documentSource = new ReaderDocumentSource(reader);
      OWLOntology ontology = manager.loadOntologyFromOntologyDocument(documentSource);
      Set<OWLLogicalAxiom> result = ontology.logicalAxioms().collect(Collectors.toSet());
      manager.removeOntology(ontology);
      return result;
    }
  }

  public void save() throws IOException, OWLOntologyStorageException, OWLOntologyCreationException {
    saveCatalystInfo();
    if (this.positives != null) {
      savePositives();
    }
    if (this.negatives != null) {
      saveNegatives();
    }
  }

  private void saveCatalystInfo() throws IOException {

    String baseName = name + "_" + iteration;

    Path targetOntologyPath = Paths.get(outputDir.toString(), baseName + "_onto.owl.xml");

    CaseData caseData = new CaseData(name, type, enforceCoherence, enforceConsistency,
        sourceOntologyPath, targetOntologyPath, caseName, outputDir, multiplicity, version, seed,
        iteration);
    String jsonString = mapper.writeValueAsString(caseData);

    baseName = name + "_" + ((type != null) ? type + "_" : "") + iteration;
    Path outputPath = Paths.get(outputDir.toString(), baseName + ".json");

    try (BufferedWriter writer = Files.newBufferedWriter(outputPath, StandardCharsets.UTF_8)) {
      writer.write(jsonString);
    }
  }

  private void savePositives()
      throws IOException, OWLOntologyStorageException, OWLOntologyCreationException {
    String baseName = name + "_" + type + "_" + iteration;
    Path outputPath = Paths.get(outputDir.toString(), baseName + "_pos.owl.xml");

    try (BufferedWriter writer = Files.newBufferedWriter(outputPath, StandardCharsets.UTF_8)) {
      OWLOntologyDocumentTarget documentTarget = new WriterDocumentTarget(writer);
      OWLOntology positives = this.manager
          .createOntology(this.positives.parallelStream().map(axiom -> (OWLAxiom) axiom));
      positives.saveOntology(new OWLXMLDocumentFormat(), documentTarget);
      manager.removeOntology(positives);
    }
  }

  private void saveNegatives()
      throws IOException, OWLOntologyStorageException, OWLOntologyCreationException {
    String baseName = name + "_" + type + "_" + iteration;
    Path outputPath = Paths.get(outputDir.toString(), baseName + "_neg.owl.xml");

    try (BufferedWriter writer = Files.newBufferedWriter(outputPath, StandardCharsets.UTF_8)) {
      OWLOntologyDocumentTarget documentTarget = new WriterDocumentTarget(writer);
      OWLOntology negatives = this.manager
          .createOntology(this.negatives.parallelStream().map(axiom -> (OWLAxiom) axiom));
      negatives.saveOntology(new OWLXMLDocumentFormat(), documentTarget);
      manager.removeOntology(negatives);
    }
  }

  public String getName() {
    return name;
  }

  public String getType() {
    return type;
  }

  public Optional<Boolean> getEnforceCoherence() {
    return Optional.ofNullable(enforceCoherence);
  }

  public Optional<Boolean> getEnforceConsistency() {
    return Optional.ofNullable(enforceConsistency);
  }

  public Set<OWLLogicalAxiom> getPositives() {
    return positives;
  }

  public Set<OWLLogicalAxiom> getNegatives() {
    return negatives;
  }

  public OWLOntology getModifiedOntology() {
    return modifiedOntology;
  }

  public Path getSourceOntologyPath() {
    return sourceOntologyPath;
  }

  public String getCaseName() {
    return caseName;
  }

  public Path getOutputDir() {
    return outputDir;
  }

  public ObjectMapper getMapper() {
    return mapper;
  }

  public OWLOntologyManager getManager() {
    return manager;
  }

  public static class CaseData {

    protected final String name;
    protected final String type;
    protected final Boolean enforceCoherence;
    protected final Boolean enforceConsistency;
    protected final Path sourceOntologyPath;
    protected final Path targetOntologyPath;
    protected final String caseName;
    protected final Path outputDir;
    protected final int multiplicity;
    protected final String version;
    protected final long seed;
    protected final int iteration;


    @JsonCreator
    public CaseData(
        @JsonProperty(value = "name") String name,
        @JsonProperty(value = "type") String type,
        @JsonProperty(value = "coherence") Boolean enforceCoherence,
        @JsonProperty(value = "consistency") Boolean enforceConsistency,
        @JsonProperty(value = "source") Path sourceOntologyPath,
        @JsonProperty(value = "target") Path targetOntologyPath,
        @JsonProperty(value = "case") String caseName,
        @JsonProperty(value = "output") Path outputDir,
        @JsonProperty(value = "multiplicity") int multiplicity,
        @JsonProperty(value = "version") String version,
        @JsonProperty(value = "seed") long seed,
        @JsonProperty(value = "iteration") int iteration) {
      this.name = name;
      this.type = type;
      this.enforceCoherence = enforceCoherence;
      this.enforceConsistency = enforceConsistency;
      this.sourceOntologyPath = sourceOntologyPath;
      this.targetOntologyPath = targetOntologyPath;
      this.caseName = caseName;
      this.outputDir = outputDir;
      this.multiplicity = multiplicity;
      this.version = version;
      this.seed = seed;
      this.iteration = iteration;
    }

    @JsonProperty("name")
    public String getName() {
      return name;
    }

    @JsonProperty("type")
    public String getType() {
      return type;
    }

    @JsonProperty("coherence")
    public Boolean getEnforceCoherence() {
      return enforceCoherence;
    }

    @JsonProperty("consistency")
    public Boolean getEnforceConsistency() {
      return enforceConsistency;
    }

    @JsonProperty("source")
    public Path getSourceOntologyPath() {
      return sourceOntologyPath;
    }

    @JsonProperty("target")
    public Path getTargetOntologyPath() {
      return targetOntologyPath;
    }

    @JsonProperty("case")
    public String getCaseName() {
      return caseName;
    }

    @JsonProperty("output")
    public Path getOutputDir() {
      return outputDir;
    }

    @JsonProperty("multiplicity")
    public int getMultiplicity() {
      return multiplicity;
    }

    @JsonProperty("version")
    public String getVersion() {
      return version;
    }

    @JsonProperty("seed")
    public long getSeed() {
      return seed;
    }

    @JsonProperty("iteration")
    public int getIteration() {
      return iteration;
    }
  }
}
