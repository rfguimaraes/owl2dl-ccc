/*
 *    Copyright 2018-2019,2023 OWL 2 DL - Change Case Creator Developers
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package br.usp.ime.owl2dlccc;

import br.usp.ime.owl2dlccc.model.AxiomRequirement;
import br.usp.ime.owl2dlccc.model.CompositeTerm;
import br.usp.ime.owl2dlccc.model.PrimitiveTerm;
import br.usp.ime.owl2dlccc.model.Term;
import java.util.List;
import java.util.Optional;
import java.util.SortedMap;
import java.util.TreeMap;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import org.semanticweb.owlapi.model.OWLClassExpression;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class FullMatchFactory implements CompleteMatchIterator {

  private static Logger logger = LoggerFactory.getLogger(FullMatchFactory.class);

  protected final List<PrimitiveTerm> primitiveTerms;
  protected final List<CompositeTerm> compositeTerms;
  // TODO: consider splitting this into a map for primitives and other for composites
  private final SortedMap<String, Term> nameToTerm;
  private final PrimitiveMatchIterator primitiveMatchIterator;

  public FullMatchFactory(List<PrimitiveTerm> primitiveTerms,
      List<CompositeTerm> compositeTerms,
      DistinctnessPolicy distinctnessPolicy, DomainComputer domainComputer, long seed) {
    this.primitiveTerms = primitiveTerms;
    this.compositeTerms = compositeTerms;
    this.nameToTerm = buildNameToTerm();
    primitiveMatchIterator = new PrimitiveMatchFactory(primitiveTerms, domainComputer,
        distinctnessPolicy, null, seed);

  }

  private SortedMap<String, Term> buildNameToTerm() {
    SortedMap<String, Term> map = new TreeMap<>();

    primitiveTerms.forEach(t -> map.put(t.getName(), t));
    compositeTerms.forEach(t -> map.put(t.getName(), t));

    return map;
  }

  @Override
  public void update(AxiomRequirement axiomRequirement) {
    primitiveMatchIterator.update(
        axiomRequirement.getAxiomSpecification().getArguments().stream().map(nameToTerm::get)
            .flatMap(this::getOperands).collect(Collectors.toSet()));
  }

  private Stream<Term> getOperands(Term term) {
    return term.getOperands().stream().map(nameToTerm::get);
  }

  @Override
  public boolean hasNext() {
    return primitiveMatchIterator.hasNext();
  }

  @Override
  public Match next() {
    Match next = primitiveMatchIterator.next();
    if (next == null) {
      return null;
    }

    CompositeTermBuilder compositeTermBuilder = new CompositeTermBuilder(next);
    for (CompositeTerm compositeTerm : compositeTerms) {
      String key = compositeTerm.getName();
      Optional<OWLClassExpression> optClassExpression = compositeTermBuilder
          .buildClassExpression(compositeTerm);
      optClassExpression.ifPresent(x -> next.setClassExpression(key, x));
    }
    return next;
  }
}

