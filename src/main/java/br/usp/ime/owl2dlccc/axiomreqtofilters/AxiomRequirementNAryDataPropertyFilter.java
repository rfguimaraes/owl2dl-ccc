/*
 *    Copyright 2018-2019,2023 OWL 2 DL - Change Case Creator Developers
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package br.usp.ime.owl2dlccc.axiomreqtofilters;

import com.google.common.collect.BiMap;
import java.util.List;
import org.semanticweb.owlapi.model.OWLClass;
import org.semanticweb.owlapi.model.OWLClassExpression;
import org.semanticweb.owlapi.model.OWLDataPropertyAxiom;
import org.semanticweb.owlapi.model.OWLDataPropertyExpression;
import org.semanticweb.owlapi.model.OWLOntology;
import org.semanticweb.owlapi.reasoner.OWLReasoner;

public abstract class AxiomRequirementNAryDataPropertyFilter<T extends OWLDataPropertyAxiom> extends
    AxiomRequirementFilter implements AxiomRequirementDataPropertyFilter {

  public AxiomRequirementNAryDataPropertyFilter(OWLOntology ontology, OWLReasoner reasoner,
      BiMap<OWLClass, OWLClassExpression> extensionBiMap) {
    super(ontology, reasoner, extensionBiMap);
  }

  protected abstract T buildAxiom(List<OWLDataPropertyExpression> properties);

}
