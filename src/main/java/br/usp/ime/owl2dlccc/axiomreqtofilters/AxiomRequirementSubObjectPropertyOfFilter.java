/*
 *    Copyright 2018-2019,2023 OWL 2 DL - Change Case Creator Developers
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package br.usp.ime.owl2dlccc.axiomreqtofilters;

import br.usp.ime.owl2dlccc.Match;
import br.usp.ime.owl2dlccc.model.AxiomRequirement;
import br.usp.ime.owl2dlccc.model.PrimitiveTerm;
import com.google.common.collect.BiMap;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;
import org.semanticweb.owlapi.model.AsOWLDataProperty;
import org.semanticweb.owlapi.model.AxiomType;
import org.semanticweb.owlapi.model.OWLClass;
import org.semanticweb.owlapi.model.OWLClassExpression;
import org.semanticweb.owlapi.model.OWLDataPropertyExpression;
import org.semanticweb.owlapi.model.OWLObjectPropertyExpression;
import org.semanticweb.owlapi.model.OWLOntology;
import org.semanticweb.owlapi.model.OWLSubObjectPropertyOfAxiom;
import org.semanticweb.owlapi.model.OWLSubPropertyAxiom;
import org.semanticweb.owlapi.reasoner.OWLReasoner;
import org.semanticweb.owlapi.search.EntitySearcher;
import uk.ac.manchester.cs.owl.owlapi.OWLSubObjectPropertyOfAxiomImpl;

public class AxiomRequirementSubObjectPropertyOfFilter extends
    AxiomRequirementNAryObjectPropertyFilter<OWLSubObjectPropertyOfAxiom> {

  public AxiomRequirementSubObjectPropertyOfFilter(OWLOntology ontology, OWLReasoner reasoner,
      BiMap<OWLClass, OWLClassExpression> extensionBiMap) {
    super(ontology, reasoner, extensionBiMap);
  }

  @Override
  protected OWLSubObjectPropertyOfAxiom buildAxiom(List<OWLObjectPropertyExpression> properties) {
    return new OWLSubObjectPropertyOfAxiomImpl(properties.get(0), properties.get(1),
        Collections.emptySet());
  }

  @Override
  protected void domain(List<OWLObjectPropertyExpression> properties, PrimitiveTerm pivot,
      AxiomRequirement requirement, Match match, Set<OWLObjectPropertyExpression> domain) {

    List<String> arguments = requirement.getAxiomSpecification().getArguments();

    if (arguments.indexOf(pivot.getName()) == 0) {
      // Pivot is SubProperty

      Optional<OWLObjectPropertyExpression> optSuperProperty = match
          .getObjectPropertyExpression(arguments.get(1));

      if (requirement.isAsserted().isPresent()) {
        if (optSuperProperty.isPresent()) {
          Set<OWLDataPropertyExpression> candidates = EntitySearcher
              .getSubProperties(optSuperProperty.get(), originalOntology).filter(
                  AsOWLDataProperty::isOWLDataProperty)
              .map(AsOWLDataProperty::asOWLDataProperty).collect(
                  Collectors.toSet());
          if (requirement.isAsserted().get()) {
            domain.retainAll(candidates);
          } else {
            domain.removeAll(candidates);
          }
        } else if (requirement.isAsserted().get()) {
          domain.retainAll(originalOntology.axioms(AxiomType.SUB_OBJECT_PROPERTY)
              .map(OWLSubPropertyAxiom::getSubProperty).collect(Collectors.toSet()));
        }
      }
      if (requirement.isEntailed().isPresent() && optSuperProperty.isPresent()) {

        OWLObjectPropertyExpression objectPropertyExpression = optSuperProperty.get();

        Set<OWLObjectPropertyExpression> candidates = extendedReasoner
            .subObjectProperties(objectPropertyExpression).collect(Collectors.toSet());
        extendedReasoner.equivalentObjectProperties(objectPropertyExpression)
            .forEach(candidates::add);
        if (requirement.isEntailed().get()) {
          domain.retainAll(candidates);
        } else {
          domain.removeAll(candidates);
        }

      }
    } else {
      // Pivot is SuperProperty

      Optional<OWLObjectPropertyExpression> optSubProperty = match
          .getObjectPropertyExpression(arguments.get(0));

      if (requirement.isAsserted().isPresent()) {
        if (optSubProperty.isPresent()) {
          Set<OWLObjectPropertyExpression> candidates = EntitySearcher
              .getSuperProperties(optSubProperty.get(), originalOntology).collect(
                  Collectors.toSet());
          if (requirement.isAsserted().get()) {
            domain.retainAll(candidates);
          } else {
            domain.removeAll(candidates);
          }
        } else if (requirement.isAsserted().get()) {
          domain.retainAll(originalOntology.axioms(AxiomType.SUB_OBJECT_PROPERTY)
              .map(OWLSubPropertyAxiom::getSuperProperty).collect(Collectors.toSet()));
        }
      }
      if (requirement.isEntailed().isPresent() && optSubProperty.isPresent()) {

        OWLObjectPropertyExpression objectPropertyExpression = optSubProperty.get();

        Set<OWLObjectPropertyExpression> candidates = extendedReasoner
            .superObjectProperties(objectPropertyExpression).collect(Collectors.toSet());
        extendedReasoner.equivalentObjectProperties(objectPropertyExpression)
            .forEach(candidates::add);
        if (requirement.isEntailed().get()) {
          domain.retainAll(candidates);
        } else {
          domain.removeAll(candidates);
        }
      }
    }
  }
}
