/*
 *    Copyright 2018-2019,2023 OWL 2 DL - Change Case Creator Developers
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package br.usp.ime.owl2dlccc.axiomreqtofilters;

import br.usp.ime.owl2dlccc.Match;
import br.usp.ime.owl2dlccc.model.AxiomRequirement;
import br.usp.ime.owl2dlccc.model.PrimitiveTerm;
import com.google.common.collect.BiMap;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import org.semanticweb.owlapi.model.OWLClass;
import org.semanticweb.owlapi.model.OWLClassAxiom;
import org.semanticweb.owlapi.model.OWLClassExpression;
import org.semanticweb.owlapi.model.OWLOntology;
import org.semanticweb.owlapi.reasoner.OWLReasoner;

public abstract class AxiomRequirementClassAxiomFilter<T extends OWLClassAxiom> extends
    AxiomRequirementFilter implements AxiomRequirementClassFilter {

  public AxiomRequirementClassAxiomFilter(OWLOntology ontology,
      OWLReasoner reasoner, BiMap<OWLClass, OWLClassExpression> extensionBiMap) {
    super(ontology, reasoner, extensionBiMap);
  }

  @Override
  public void classFilter(PrimitiveTerm pivot, AxiomRequirement requirement, Match match,
      Set<OWLClassExpression> domain) {
    List<String> arguments = requirement.getAxiomSpecification().getArguments();
    List<OWLClassExpression> classes = arguments.stream()
        .map(match::getClassExpression)
        .flatMap(o -> o.map(Stream::of).orElseGet(Stream::empty))
        .collect(Collectors.toList());
    domain(classes, pivot, requirement, match, domain);
  }

  private Optional<OWLClassExpression> getOwlClassExpression(OWLClassExpression classExpression,
      PrimitiveTerm pivot, Match match, String arg) {
    if (arg.equals(pivot.getName())) {
      return Optional.of(classExpression);
    } else {
      return match.getClassExpression(arg);
    }
  }

  protected abstract T buildAxiom(List<OWLClassExpression> classes);

  protected abstract void domain(List<OWLClassExpression> classes, PrimitiveTerm pivot,
      AxiomRequirement requirement, Match match,
      Set<OWLClassExpression> domain);
}
