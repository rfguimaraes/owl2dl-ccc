/*
 *    Copyright 2018-2019,2023 OWL 2 DL - Change Case Creator Developers
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package br.usp.ime.owl2dlccc.axiomreqtofilters;

import br.usp.ime.owl2dlccc.Match;
import br.usp.ime.owl2dlccc.model.AxiomRequirement;
import br.usp.ime.owl2dlccc.model.PrimitiveTerm;
import com.google.common.collect.BiMap;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import org.semanticweb.owlapi.model.AxiomType;
import org.semanticweb.owlapi.model.OWLClass;
import org.semanticweb.owlapi.model.OWLClassExpression;
import org.semanticweb.owlapi.model.OWLDataPropertyExpression;
import org.semanticweb.owlapi.model.OWLEntity;
import org.semanticweb.owlapi.model.OWLHasKeyAxiom;
import org.semanticweb.owlapi.model.OWLObjectPropertyExpression;
import org.semanticweb.owlapi.model.OWLOntology;
import org.semanticweb.owlapi.reasoner.OWLReasoner;

public class AxiomRequirementHasKeyFilter extends AxiomRequirementFilter implements
    AxiomRequirementClassFilter, AxiomRequirementObjectPropertyFilter,
    AxiomRequirementDataPropertyFilter {

  public AxiomRequirementHasKeyFilter(OWLOntology ontology,
      OWLReasoner reasoner, BiMap<OWLClass, OWLClassExpression> extensionBiMap) {
    super(ontology, reasoner, extensionBiMap);
  }

  @Override
  public void classFilter(PrimitiveTerm pivot, AxiomRequirement requirement, Match match,
      Set<OWLClassExpression> domain) {

    Set<OWLEntity> entities = getEntities(requirement, match);

    if (requirement.isAsserted().orElse(false)) {
      domain.retainAll(getHasKeyAxiomsWithSignature(entities)
          .map(OWLHasKeyAxiom::getClassExpression).collect(Collectors.toSet()));
    }
  }

  private Stream<OWLHasKeyAxiom> getHasKeyAxiomsWithSignature(Set<OWLEntity> entities) {
    return originalOntology.axioms(AxiomType.HAS_KEY)
        .filter(axiom -> entities.stream().allMatch(axiom::containsEntityInSignature));
  }

  @Override
  public void dataPropertyFilter(PrimitiveTerm pivot, AxiomRequirement requirement, Match match,
      Set<OWLDataPropertyExpression> domain) {
    Set<OWLEntity> entities = getEntities(requirement, match);

    if (requirement.isAsserted().orElse(false)) {
      domain.retainAll(getHasKeyAxiomsWithSignature(entities)
          .flatMap(OWLHasKeyAxiom::dataPropertyExpressions).collect(Collectors.toSet()));
    }
  }

  private Set<OWLEntity> getEntities(AxiomRequirement requirement, Match match) {
    return requirement.getAxiomSpecification().getArguments().stream()
        .map(match::getEntity)
        .flatMap(o -> o.map(Stream::of).orElseGet(Stream::empty))
        .collect(Collectors.toSet());
  }

  @Override
  public void objectPropertyFilter(PrimitiveTerm pivot, AxiomRequirement requirement, Match match,
      Set<OWLObjectPropertyExpression> domain) {
    Set<OWLEntity> entities = getEntities(requirement, match);

    if (requirement.isAsserted().orElse(false)) {
      domain.retainAll(getHasKeyAxiomsWithSignature(entities)
          .flatMap(OWLHasKeyAxiom::objectPropertyExpressions).collect(Collectors.toSet()));
    }
  }
}
