/*
 *    Copyright 2018-2019,2023 OWL 2 DL - Change Case Creator Developers
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package br.usp.ime.owl2dlccc.axiomreqtofilters;

import br.usp.ime.owl2dlccc.Match;
import br.usp.ime.owl2dlccc.model.AxiomRequirement;
import br.usp.ime.owl2dlccc.model.PrimitiveTerm;
import com.google.common.collect.BiMap;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;
import org.semanticweb.owlapi.model.AsOWLClass;
import org.semanticweb.owlapi.model.AxiomType;
import org.semanticweb.owlapi.model.OWLClass;
import org.semanticweb.owlapi.model.OWLClassExpression;
import org.semanticweb.owlapi.model.OWLEquivalentClassesAxiom;
import org.semanticweb.owlapi.model.OWLNaryClassAxiom;
import org.semanticweb.owlapi.model.OWLOntology;
import org.semanticweb.owlapi.reasoner.OWLReasoner;
import org.semanticweb.owlapi.search.EntitySearcher;
import uk.ac.manchester.cs.owl.owlapi.OWLEquivalentClassesAxiomImpl;

public class AxiomRequirementEquivalentClassesFilter extends
    AxiomRequirementClassAxiomFilter<OWLEquivalentClassesAxiom> implements
    AxiomRequirementClassFilter {

  public AxiomRequirementEquivalentClassesFilter(OWLOntology ontology,
      OWLReasoner reasoner, BiMap<OWLClass, OWLClassExpression> extensionBiMap) {
    super(ontology, reasoner, extensionBiMap);
  }

  @Override
  protected OWLEquivalentClassesAxiom buildAxiom(List<OWLClassExpression> classes) {
    return new OWLEquivalentClassesAxiomImpl(classes, Collections.emptySet());
  }

  @Override
  protected void domain(List<OWLClassExpression> classes, PrimitiveTerm pivot,
      AxiomRequirement requirement, Match match, Set<OWLClassExpression> domain) {

    if (requirement.isAsserted().isPresent()) {
      if (requirement.isAsserted().get()) {
        if (classes.isEmpty()) {
          // All undefined, but we can still look at those referenced by some axiom of that type
          domain.retainAll(originalOntology.axioms(AxiomType.EQUIVALENT_CLASSES)
              .flatMap(OWLNaryClassAxiom::classExpressions).collect(Collectors.toSet()));
        } else {
          Optional<OWLClassExpression> optClass = classes.stream().filter(AsOWLClass::isOWLClass)
              .findAny();
          optClass.ifPresent(owlClassExpression -> domain.retainAll(
              EntitySearcher.getEquivalentClasses(owlClassExpression.asOWLClass(), originalOntology)
                  .collect(Collectors.toSet())));
        }
      } else if (!classes.isEmpty()) {
        // It can't be any other CE that appears in a equivalence with one of the others
        domain.removeAll(originalOntology.axioms(AxiomType.EQUIVALENT_CLASSES)
            .filter(ax -> classes.stream().anyMatch(ax::contains))
            .flatMap(OWLNaryClassAxiom::classExpressions).collect(Collectors.toSet()));
      }
    }

    // Since we are looking at equivalences, as long as we have one defined we can filter
    // both positive and negative entailment requests.
    if (requirement.isEntailed().isPresent() && !classes.isEmpty()) {
      Set<OWLClassExpression> equivalents = extendedReasoner.equivalentClasses(classes.get(0))
          .map(this::revert)
          .collect(Collectors.toSet());
      if (requirement.isEntailed().get()) {
        domain.retainAll(equivalents);
      } else {
        domain.removeAll(equivalents);
      }
    }
  }
}
