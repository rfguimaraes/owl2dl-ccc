/*
 *    Copyright 2018-2019,2023 OWL 2 DL - Change Case Creator Developers
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package br.usp.ime.owl2dlccc.axiomreqtofilters;

import br.usp.ime.owl2dlccc.Match;
import br.usp.ime.owl2dlccc.model.AxiomRequirement;
import br.usp.ime.owl2dlccc.model.PrimitiveTerm;
import com.google.common.collect.BiMap;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import org.semanticweb.owlapi.model.AxiomType;
import org.semanticweb.owlapi.model.HasProperty;
import org.semanticweb.owlapi.model.HasRange;
import org.semanticweb.owlapi.model.OWLClass;
import org.semanticweb.owlapi.model.OWLClassExpression;
import org.semanticweb.owlapi.model.OWLObjectPropertyExpression;
import org.semanticweb.owlapi.model.OWLObjectPropertyRangeAxiom;
import org.semanticweb.owlapi.model.OWLOntology;
import org.semanticweb.owlapi.reasoner.OWLReasoner;
import org.semanticweb.owlapi.search.EntitySearcher;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.ac.manchester.cs.owl.owlapi.OWLObjectPropertyRangeAxiomImpl;

public class AxiomRequirementObjectPropertyRangeFilter extends
    AxiomRequirementFilter implements AxiomRequirementObjectPropertyFilter,
    AxiomRequirementClassFilter {

  private static final Logger logger = LoggerFactory
      .getLogger(AxiomRequirementObjectPropertyRangeFilter.class);

  public AxiomRequirementObjectPropertyRangeFilter(OWLOntology ontology,
      OWLReasoner reasoner, BiMap<OWLClass, OWLClassExpression> extensionBiMap) {
    super(ontology, reasoner, extensionBiMap);
  }

  @Override
  public void objectPropertyFilter(PrimitiveTerm pivot, AxiomRequirement requirement, Match match,
      Set<OWLObjectPropertyExpression> domain) {
    List<String> arguments = requirement.getAxiomSpecification().getArguments();
    if (arguments.indexOf(pivot.getName()) != 0) {
      logger.debug("Pivot is not a object property expression. Skipping...");
      return;
    }

    // Get the range
    Optional<OWLClassExpression> optRange = match
        .getClassExpression(requirement.getAxiomSpecification().getArguments().get(1));
    if (requirement.isAsserted().isPresent()) {
      Stream<OWLObjectPropertyRangeAxiom> axioms = originalOntology
          .axioms(AxiomType.OBJECT_PROPERTY_RANGE);

      if (optRange.isPresent()) {
        axioms = axioms.filter(ax -> ax.getRange().equals(optRange.get()));
      }

      Set<OWLObjectPropertyExpression> dataProperties = axioms.map(HasProperty::getProperty)
          .collect(Collectors.toSet());
      if (requirement.isAsserted().get()) {
        domain.retainAll(dataProperties);
      } else {
        domain.removeAll(dataProperties);
      }

    }

    if (requirement.isEntailed().isPresent() && optRange.isPresent()) {
      Set<OWLObjectPropertyExpression> objectProperties = domain.stream().filter(
          property -> extendedReasoner.isEntailed(
              new OWLObjectPropertyRangeAxiomImpl(property, optRange.get(),
                  Collections.emptySet()))).collect(Collectors.toSet());
      if (requirement.isEntailed().get()) {
        domain.retainAll(objectProperties);
      } else {
        domain.removeAll(objectProperties);
      }
    }
  }

  @Override
  public void classFilter(PrimitiveTerm pivot, AxiomRequirement requirement, Match match,
      Set<OWLClassExpression> domain) {
    List<String> arguments = requirement.getAxiomSpecification().getArguments();

    if (arguments.indexOf(pivot.getName()) != 1) {
      logger.debug("Pivot is not a class expression. Skipping...");
      return;
    }

    // Get the property
    Optional<OWLObjectPropertyExpression> optPropertyExpression = match
        .getObjectPropertyExpression(requirement.getAxiomSpecification().getArguments().get(0));

    if (requirement.isAsserted().isPresent()) {

      if (optPropertyExpression.isPresent() && optPropertyExpression.get().isOWLDataProperty()) {

        Set<OWLClassExpression> propertyRanges = EntitySearcher
            .getRanges(optPropertyExpression.get(), originalOntology)
            .collect(Collectors.toSet());
        if (requirement.isAsserted().get()) {
          domain.retainAll(propertyRanges);
        } else {
          domain.removeAll(propertyRanges);
        }

      } else if (requirement.isAsserted().get()) {
        domain.retainAll(
            originalOntology.axioms(AxiomType.OBJECT_PROPERTY_RANGE).map(HasRange::getRange)
                .collect(Collectors.toSet()));
      }
    }
    if (requirement.isEntailed().isPresent() && optPropertyExpression.isPresent()
        && optPropertyExpression.get().isOWLDataProperty()) {
      Set<OWLClassExpression> propertyRanges = extendedReasoner
          .objectPropertyRanges(optPropertyExpression.get()).map(this::revert).collect(
              Collectors.toSet());
      if (requirement.isEntailed().get()) {
        domain.retainAll(propertyRanges);
      } else {
        domain.removeAll(propertyRanges);
      }
    }
  }
}
