/*
 *    Copyright 2018-2019,2023 OWL 2 DL - Change Case Creator Developers
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package br.usp.ime.owl2dlccc;

import br.usp.ime.owl2dlccc.axiomspectoaxiom.AxiomSpecificationToOWLAxiomTranslator;
import br.usp.ime.owl2dlccc.model.AxiomRequirement;
import br.usp.ime.owl2dlccc.model.IntegrityConstraints;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import org.semanticweb.owlapi.model.OWLLogicalAxiom;
import org.semanticweb.owlapi.model.OWLOntology;
import org.semanticweb.owlapi.model.parameters.Imports;
import org.semanticweb.owlapi.reasoner.InconsistentOntologyException;
import org.semanticweb.owlapi.reasoner.OWLReasoner;

public class ConditionChecker {

  private final OWLOntology ontology;
  private final OWLReasoner reasoner;
  private final Match match;
  private final Map<AxiomRequirement, OWLLogicalAxiom> axiomCache = new HashMap<>();

  public ConditionChecker(OWLOntology ontology, OWLReasoner reasoner, Match match) {
    this.ontology = ontology;
    this.reasoner = reasoner;
    this.match = match;
  }

  private boolean isCoherent() {
    try {
      return !reasoner.unsatisfiableClasses().findAny().isPresent();
    } catch (InconsistentOntologyException e) {
      return !this.ontology.classesInSignature(Imports.INCLUDED).findAny().isPresent();
    }
  }

  public ConditionCheckResult check(IntegrityConstraints integrityConstraints) {

    if (integrityConstraints.enforceCoherence().isPresent()) {
      if (integrityConstraints.enforceCoherence().get() != isCoherent()) {
        return new ConditionCheckResult(false, CheckPhase.COHERENCE, null);
      }
    }

    if (integrityConstraints.enforceConsistency().isPresent()) {
      if (integrityConstraints.enforceConsistency().get() != reasoner.isConsistent()) {
        return new ConditionCheckResult(false, CheckPhase.CONSISTENCY, null);
      }
    }

    List<AxiomRequirement> axiomRequirements = integrityConstraints.getAxiomRequirements()
        .orElse(Collections.emptyList());
    axiomRequirements.forEach(this::cache);

    for (AxiomRequirement axiomRequirement : axiomRequirements) {
      if (!assertionStatusOk(axiomRequirement)) {
        return new ConditionCheckResult(false, CheckPhase.ASSERTION, axiomRequirement);
      } else if (!entailmentStatusOk(axiomRequirement)) {
        return new ConditionCheckResult(false, CheckPhase.ENTAILMENT, axiomRequirement);
      }
    }

    return new ConditionCheckResult(true, null, null);
  }

  private void cache(AxiomRequirement axiomRequirement) {
    Optional<? extends OWLLogicalAxiom> optAxiom = AxiomSpecificationToOWLAxiomTranslator
        .translate(axiomRequirement.getAxiomSpecification(), match);

    optAxiom.ifPresent(owlLogicalAxiom -> axiomCache.put(axiomRequirement, owlLogicalAxiom));
  }

  private boolean assertionStatusOk(AxiomRequirement axiomRequirement) {

    Optional<Boolean> asserted = axiomRequirement.isAsserted();
    OWLLogicalAxiom axiom = axiomCache.get(axiomRequirement);

    // TODO: decide whether to include the imports
    return !asserted.isPresent() || (ontology.logicalAxioms(Imports.INCLUDED)
        .anyMatch(axiom1 -> axiom1.equalsIgnoreAnnotations(axiom)) == asserted.get());

  }

  private boolean entailmentStatusOk(AxiomRequirement axiomRequirement) {

    Optional<Boolean> entailed = axiomRequirement.isEntailed();
    OWLLogicalAxiom axiom = axiomCache.get(axiomRequirement);

    if (!entailed.isPresent()) {
      return true;
    }

    try {
      return reasoner.isEntailed(axiom) == entailed.get();
    } catch (InconsistentOntologyException e) {
      return entailed.get();
    }

  }

  public enum CheckPhase {
    COHERENCE,
    CONSISTENCY,
    ASSERTION,
    ENTAILMENT
  }

  public class ConditionCheckResult {

    private final boolean passed;
    private final CheckPhase checkPhase;
    private final AxiomRequirement failedRequirement;

    public ConditionCheckResult(boolean passed,
        CheckPhase checkPhase,
        AxiomRequirement failedRequirement) {
      this.passed = passed;
      this.checkPhase = checkPhase;
      this.failedRequirement = failedRequirement;
    }

    public boolean passed() {
      return passed;
    }

    public CheckPhase getCheckPhase() {
      return checkPhase;
    }

    public AxiomRequirement getFailedRequirement() {
      return failedRequirement;
    }
  }

}
