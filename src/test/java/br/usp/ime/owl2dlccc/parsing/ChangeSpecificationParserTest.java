/*
 *    Copyright 2018-2020,2023 OWL 2 DL - Change Case Creator Developers
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package br.usp.ime.owl2dlccc.parsing;

import static br.usp.ime.owl2dlccc.parsing.ChangeSpecificationParser.parseChangeSpecification;
import static org.junit.jupiter.api.Assertions.*;

import br.usp.ime.owl2dlccc.model.ActionBlock;
import br.usp.ime.owl2dlccc.model.ActionBlockImpl;
import br.usp.ime.owl2dlccc.model.AxiomRequirement;
import br.usp.ime.owl2dlccc.model.AxiomRequirementImpl;
import br.usp.ime.owl2dlccc.model.AxiomSpecification;
import br.usp.ime.owl2dlccc.model.AxiomSpecificationImpl;
import br.usp.ime.owl2dlccc.model.CatalystBlock;
import br.usp.ime.owl2dlccc.model.CatalystBlockImpl;
import br.usp.ime.owl2dlccc.model.ChangeSpecification;
import br.usp.ime.owl2dlccc.model.ChangeSpecificationImpl;
import br.usp.ime.owl2dlccc.model.CompositeTerm;
import br.usp.ime.owl2dlccc.model.CompositeTermImpl;
import br.usp.ime.owl2dlccc.model.HeaderBlock;
import br.usp.ime.owl2dlccc.model.HeaderBlockImpl;
import br.usp.ime.owl2dlccc.model.IntegrityConstraints;
import br.usp.ime.owl2dlccc.model.IntegrityConstraintsImpl;
import br.usp.ime.owl2dlccc.model.OWL2DLConstructor;
import br.usp.ime.owl2dlccc.model.OWL2DLFragment;
import br.usp.ime.owl2dlccc.model.PrimitiveTerm;
import br.usp.ime.owl2dlccc.model.PrimitiveTermImpl;
import br.usp.ime.owl2dlccc.model.TermsBlock;
import br.usp.ime.owl2dlccc.model.TermsBlockImpl;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.Version;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleAbstractTypeResolver;
import com.fasterxml.jackson.databind.module.SimpleModule;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import java.io.IOException;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.semanticweb.owlapi.model.AxiomType;
import org.semanticweb.owlapi.model.EntityType;

class ChangeSpecificationParserTest {

  SimpleModule typeModule;

  @BeforeEach
  void setUp() {

    SimpleAbstractTypeResolver typeResolver = new SimpleAbstractTypeResolver();
    typeResolver.addMapping(ChangeSpecification.class, ChangeSpecificationImpl.class);
    typeResolver.addMapping(HeaderBlock.class, HeaderBlockImpl.class);
    typeResolver.addMapping(TermsBlock.class, TermsBlockImpl.class);
    typeResolver
        .addMapping(PrimitiveTerm.class, PrimitiveTermImpl.class);
    typeResolver
        .addMapping(CompositeTerm.class, CompositeTermImpl.class);
    typeResolver.addMapping(AxiomRequirement.class, AxiomRequirementImpl.class);
    typeResolver.addMapping(AxiomSpecification.class, AxiomSpecificationImpl.class);
    typeResolver.addMapping(ActionBlock.class, ActionBlockImpl.class);
    typeResolver.addMapping(CatalystBlock.class, CatalystBlockImpl.class);
    typeResolver.addMapping(IntegrityConstraints.class, IntegrityConstraintsImpl.class);

    typeModule = new SimpleModule("Test Type Mapping", Version.unknownVersion());
    typeModule.setAbstractTypes(typeResolver);
    typeModule.addSerializer(AxiomType.class, new AxiomTypeSerializer());
    typeModule.addDeserializer(AxiomType.class, new AxiomTypeDeserializer());
    typeModule.addSerializer(EntityType.class, new EntityTypeSerializer());
    typeModule.addDeserializer(EntityType.class, new EntityTypeDeserializer());
  }

  @Test
  void parseChangeSpecification_shouldWorkForMinimalSpecification() throws IOException {
    String minimalChangeSpecificationJSON = "{\n"
        + "    \"header\" : {\n"
        + "        \"name\" : \"MinimalAcceptableExample\"\n"
        + "    }\n"
        + "\n"
        + "}";

    ChangeSpecification changeSpecification = parseChangeSpecification(
        minimalChangeSpecificationJSON, typeModule);
    assertNotNull(changeSpecification);
    assertEquals(changeSpecification.getHeaderBlock().getName(), "MinimalAcceptableExample");
  }

  @Test
  void parseChangeSpecification_shouldWorkWithFullHeader() throws IOException {
    String fullHeaderChangeSpecificationJSON = "{\n"
        + "    \"header\" : {\n"
        + "        \"name\" : \"MinimalFullHeader\",\n"
        + "        \"author\" : \"Author Name\",\n"
        + "        \"input_fragment\" : \"DL\",\n"
        + "        \"output_fragment\" : \"DL\",\n"
        + "        \"format_version\" : \"0.0.1\",\n"
        + "        \"description\" : \"Minimal change file with all headerBlock fields.\"\n"
        + "    }\n"
        + "}";

    ChangeSpecification changeSpecification = parseChangeSpecification(
        fullHeaderChangeSpecificationJSON, typeModule);
    assertNotNull(changeSpecification);

  }

  @Test
  void parseChangeSpecification_shouldWorkForMinimalHeaderAndDeclaration() throws IOException {
    String minHeaderAndDeclarationJSON = "{\n"
        + "    \"header\" : {\n"
        + "        \"name\" : \"MinimalAcceptableExample\"\n"
        + "    },\n"
        + "\n"
        + "    \"terms\" : {\n"
        + "        \"primitives\" : []\n"
        + "    }\n"
        + "}";

    ChangeSpecification changeSpecification = parseChangeSpecification(
        minHeaderAndDeclarationJSON, typeModule);
    assertNotNull(changeSpecification);
    assertEquals(changeSpecification.getHeaderBlock().getName(), "MinimalAcceptableExample");
    Optional<TermsBlock> optTermsBlock = changeSpecification.getTermsBlock();
    assertTrue(optTermsBlock.isPresent());
    assertTrue(optTermsBlock.get().getPrimitives().isPresent());
    assertTrue(optTermsBlock.get().getPrimitives().get().isEmpty());
  }

  @Test
  void parseChangeSpecification_shouldWorkForSimpleHeaderAndPrimitivesDeclaration()
      throws IOException {
    String simpleJSON = "{\n"
        + "    \"header\" : {\n"
        + "        \"name\" : \"MinimalAcceptableExample\"\n"
        + "    },\n"
        + "\n"
        + "    \"terms\" : {\n"
        + "        \"primitives\" : [{\"name\" : \"A\", \"entity_type\" : \"Class\", \"owl_thing\" : true}]\n"
        + "    }\n"
        + "}";

    ChangeSpecification changeSpecification = parseChangeSpecification(simpleJSON, typeModule);
    assertNotNull(changeSpecification);
    Optional<TermsBlock> optTermsBlock = changeSpecification.getTermsBlock();
    assertTrue(optTermsBlock.isPresent());
    assertEquals(changeSpecification.getHeaderBlock().getName(), "MinimalAcceptableExample");
    assertTrue(optTermsBlock.get().getPrimitives()
        .map(list -> list.stream().anyMatch(var -> var.getName().equals("A")))
        .orElse(Boolean.FALSE));
  }

  @Test
  void parseChangeSpecification_shouldWorkForSimpleHeaderAndDeclarations() throws IOException {
    String changeSpecificationJSON = "{\n"
        + "    \"header\" : {\n"
        + "        \"name\" : \"MinimalAcceptableExample\"\n"
        + "    },\n"
        + "\n"
        + "    \"terms\" : {\n"
        + "        \"primitives\" : [\n"
        + "            {\"name\" : \"A\", \"entity_type\" : \"Class\"},\n"
        + "            {\"name\" : \"B\", \"entity_type\" : \"Class\"}\n"
        + "        ],\n"
        + "        \"composites\" : [\n"
        + "            {\"name\" : \"Conj\", \"entity_type\" : \"Class\", \"constructor\" : \"Conjunction\", \"operands\" : [\"A\", \"B\"]}\n"
        + "        ]\n"
        + "\n"
        + "    }\n"
        + "}";

    ChangeSpecification changeSpecification = parseChangeSpecification(changeSpecificationJSON,
        typeModule);
    assertNotNull(changeSpecification);
    Optional<TermsBlock> optTermsBlock = changeSpecification.getTermsBlock();
    assertTrue(optTermsBlock.isPresent());

    assertTrue(optTermsBlock.get().getPrimitives()
        .map(list -> list.stream().anyMatch(var -> var.getName().equals("A")))
        .orElse(Boolean.FALSE));

    assertTrue(optTermsBlock.get().getPrimitives()
        .map(list -> list.stream().anyMatch(var -> var.getName().equals("B")))
        .orElse(Boolean.FALSE));

    assertTrue(optTermsBlock.get().getComposites().map(list -> list.stream().anyMatch(
        var ->
            var.getName().equals("Conj") &&
                var.getConstructor().equals(OWL2DLConstructor.CONJUNCTION) &&
                var.getOperands().containsAll(Sets.newHashSet("A", "B"))
    )).orElse(Boolean.FALSE));
  }

  @Test
  void parseChangeSpecification_shouldWorkWithSimplePrecondition() throws IOException {
    String changeSpecificationJSON = "{\n"
        + "    \"header\" : {\n"
        + "        \"name\" : \"MinimalAcceptableExample\"\n"
        + "    },\n"
        + "\n"
        + "    \"preconditions\" : {\n"
        + "        \"requirements\" : [{\"axiom\" : {\"axiom_type\" : \"SubClassOf\", \"arguments\" : [\"A\", \"B\"]}, \"entailed\" : true}]\n"
        + "    }\n"
        + "}\n";

    ChangeSpecification changeSpecification = parseChangeSpecification(changeSpecificationJSON,
        typeModule);
    assertNotNull(changeSpecification);
    assertTrue(changeSpecification.getPreconditions().isPresent());
    assertTrue(changeSpecification.getPreconditions().get().getAxiomRequirements().isPresent());
    assertFalse(
        changeSpecification.getPreconditions().get().getAxiomRequirements().get().isEmpty());
    assertTrue(changeSpecification.getPreconditions().get().getAxiomRequirements()
        .map(list -> list.stream().anyMatch(
            requirement ->
                requirement.isEntailed().orElse(false) &&
                    !requirement.isAsserted().isPresent() &&
                    requirement.getAxiomSpecification().getAxiomType().equals(AxiomType.SUBCLASS_OF)
        )).orElse(false));
  }

  @Test
  void parseChangeSpecification_shouldWorkWithSingleAddition() throws IOException {
    String changeSpecificationJSON = "{\n"
        + "    \"header\" : {\n"
        + "        \"name\" : \"MinimalAcceptableExample\"\n"
        + "    },\n"
        + "    \"actions\" : {\n"
        + "        \"add\" : [{\"axiom_type\" : \"SubClassOf\", \"arguments\" : [\"A\", \"B\"]}]\n"
        + "    }\n"
        + "}";

    ChangeSpecification changeSpecification = parseChangeSpecification(changeSpecificationJSON,
        typeModule);
    assertNotNull(changeSpecification);
    assertTrue(changeSpecification.getActionBlock().isPresent());
    assertTrue(changeSpecification.getActionBlock().map(actionBlock -> actionBlock.getAdditions()
        .map(list -> list.stream().anyMatch(
            axiomSpecification -> axiomSpecification.getAxiomType().equals(AxiomType.SUBCLASS_OF)))
        .orElse(Boolean.FALSE)).orElse(Boolean.FALSE));
  }

  @Test
  void parseChangeSpecification_shouldWorkWithSinglePostcondition() throws IOException {
    String changeSpecificationJSON = "{\n"
        + "    \"header\" : {\n"
        + "        \"name\" : \"MinimalAcceptableExample\"\n"
        + "    },\n"
        + "\n"
        + "    \"postconditions\" : {\n"
        + "\"requirements\" : ["
        + "        {\"axiom\" : {\"axiom_type\" : \"SubClassOf\", \"arguments\" : [\"A\", \"B\"]}, \"entailed\" : true}]\n"
        + "    }\n"
        + "}";

    ChangeSpecification changeSpecification = parseChangeSpecification(changeSpecificationJSON,
        typeModule);
    assertNotNull(changeSpecification);
    assertTrue(changeSpecification.getPostconditions().isPresent());
    assertTrue(changeSpecification.getPostconditions().get().getAxiomRequirements().isPresent());
    assertFalse(
        changeSpecification.getPostconditions().get().getAxiomRequirements().get().isEmpty());
    assertTrue(changeSpecification.getPostconditions().get().getAxiomRequirements()
        .map(list -> list.stream().anyMatch(
            requirement ->
                requirement.isEntailed().orElse(false) &&
                    !requirement.isAsserted().isPresent() &&
                    requirement.getAxiomSpecification().getAxiomType().equals(AxiomType.SUBCLASS_OF)
        )).orElse(false));
  }

  @Test
  void parseChangeSpecification_shouldWorkWithContractionTrigger() throws IOException {
    String changeSpecificationJSON = "{\n"
        + "   \"header\":{\n"
        + "      \"name\":\"MinimalAcceptableExample\"\n"
        + "   },\n"
        + "   \"catalysts\":[\n"
        + "      {\n"
        + "         \"name\":\"contraction\",\n"
        + "         \"constraints\":{\n"
        + "            \"consistency\":true,\n"
        + "            \"requirements\":[\n"
        + "               {\n"
        + "                  \"axiom\":{\n"
        + "                     \"axiom_type\":\"DisjointClasses\",\n"
        + "                     \"arguments\":[\n"
        + "                        \"A\",\n"
        + "                        \"B\"\n"
        + "                     ]\n"
        + "                  },\n"
        + "                  \"entailed\":false\n"
        + "               }\n"
        + "            ]\n"
        + "         }\n"
        + "      }\n"
        + "   ]\n"
        + "}";

    ChangeSpecification changeSpecification = parseChangeSpecification(changeSpecificationJSON,
        typeModule);
    assertNotNull(changeSpecification);
    CatalystBlock catalystBlock = changeSpecification.getCatalysts().get(0);
    assertNotNull(catalystBlock);
    assertEquals(catalystBlock.getName(), "contraction");
    assertTrue(catalystBlock.getConstraints().enforceConsistency().orElse(Boolean.FALSE));
    assertFalse(catalystBlock.getConstraints().enforceCoherence().isPresent());
    assertTrue(
        catalystBlock.getConstraints().getAxiomRequirements().orElse(Collections.emptyList())
            .stream()
            .anyMatch(axiomRequirement ->
                !axiomRequirement.isEntailed().orElse(Boolean.TRUE)
                    && !axiomRequirement.isAsserted().isPresent()
                    && axiomRequirement.getAxiomSpecification().getAxiomType()
                    .equals(AxiomType.DISJOINT_CLASSES)
            ));
  }

  @Test
  void serializationTest() throws JsonProcessingException {
    HeaderBlock headerBlock = new HeaderBlockImpl("test", null, OWL2DLFragment.DL, null, null,
        null);
    PrimitiveTerm primitiveVar1 = new PrimitiveTermImpl("A",
        EntityType.NAMED_INDIVIDUAL);
    PrimitiveTerm primitiveVar2 = new PrimitiveTermImpl("B",
        EntityType.OBJECT_PROPERTY);
    CompositeTerm compositeVar = new CompositeTermImpl("Conj",
        EntityType.CLASS, OWL2DLConstructor.CONJUNCTION, Lists.newArrayList("A", "B"));
    TermsBlock termsBlock = new TermsBlockImpl();
    termsBlock.setPrimitives(Lists.newArrayList(primitiveVar1, primitiveVar2));
    termsBlock.setComposites(Lists.newArrayList(compositeVar));
    AxiomSpecification axiomSpecification = new AxiomSpecificationImpl(AxiomType.SUBCLASS_OF,
        Lists.newArrayList("A", "B"));
    AxiomRequirement axiomRequirement = new AxiomRequirementImpl(axiomSpecification, true, null);
    List<AxiomRequirement> preconditionsList = Lists.newArrayList(axiomRequirement);
    IntegrityConstraints preconditions = new IntegrityConstraintsImpl(false, false,
        preconditionsList);
    ActionBlock actionBlock = new ActionBlockImpl(null, null);
    List<AxiomRequirement> postconditionsList = Lists.newArrayList(axiomRequirement);
    IntegrityConstraints postconditions = new IntegrityConstraintsImpl(null, null,
        postconditionsList);
    IntegrityConstraints integrityConstraints = new IntegrityConstraintsImpl(Boolean.TRUE,
        Boolean.FALSE, null);
    CatalystBlock catalystBlock = new CatalystBlockImpl("test",
        new IntegrityConstraintsImpl(true, false, null));
    ChangeSpecification changeSpecification = new ChangeSpecificationImpl(headerBlock,
        termsBlock, preconditions, actionBlock, postconditions, Lists.newArrayList(catalystBlock));

    ObjectMapper objectMapper = DefaultObjectMapper.getInstance();
    String s = objectMapper.writeValueAsString(changeSpecification);
    assertNotNull(s);
  }
}
