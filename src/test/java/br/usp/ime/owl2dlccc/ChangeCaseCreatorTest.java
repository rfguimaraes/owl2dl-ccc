/*
 *    Copyright 2018-2020,2023 OWL 2 DL - Change Case Creator Developers
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package br.usp.ime.owl2dlccc;

import br.usp.ime.owl2dlccc.model.ChangeSpecification;
import br.usp.ime.owl2dlccc.parsing.ChangeSpecificationParser;
import java.io.BufferedReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.io.TempDir;
import org.semanticweb.HermiT.ReasonerFactory;
import org.semanticweb.owlapi.apibinding.OWLManager;
import org.semanticweb.owlapi.model.OWLOntology;
import org.semanticweb.owlapi.model.OWLOntologyCreationException;
import org.semanticweb.owlapi.model.OWLOntologyManager;
import org.semanticweb.owlapi.model.OWLOntologyStorageException;
import org.semanticweb.owlapi.reasoner.OWLReasonerFactory;
import uk.ac.manchester.cs.jfact.JFactFactory;

import static org.junit.jupiter.api.Assertions.assertTrue;

class ChangeCaseCreatorTest {

  @TempDir
  Path tempDir;

  @Test
  void applyTest()
      throws IOException, OWLOntologyCreationException, SpecificationValidationException, OWLOntologyStorageException {

    Path ontologyPath = Paths.get("src", "test", "resources", "alpha_full.owl.xml");
    OWLOntologyManager manager = OWLManager.createOWLOntologyManager();
    OWLOntology ontology = manager
        .loadOntologyFromOntologyDocument(Files.newInputStream(ontologyPath));

    Path specificationPath = Paths.get("src", "test", "resources", "fullExample.json");

    try (BufferedReader reader = Files.newBufferedReader(specificationPath)) {
      ChangeSpecification changeSpecification = ChangeSpecificationParser
          .parseChangeSpecification(reader);
    int multiplicity = 1;

    OWLReasonerFactory reasonerFactory = new ReasonerFactory();

    assertTrue(ChangeCaseCreator.apply(ontology, ontologyPath.toString(), changeSpecification, reasonerFactory,
        this.tempDir, "test",
        new DistinctnessPolicy(multiplicity), 2, 0, null));
    }
  }

  @Test
  void movieExContractionTest()
      throws IOException, OWLOntologyCreationException, SpecificationValidationException, OWLOntologyStorageException {

    Path ontologyPath = Paths.get("src", "test", "resources", "movie_ex.owl.xml");
    OWLOntologyManager manager = OWLManager.createOWLOntologyManager();
    OWLOntology ontology = manager
        .loadOntologyFromOntologyDocument(Files.newInputStream(ontologyPath));

    Path specificationPath = Paths.get("src", "test", "resources", "contractionExample.json");

    try (BufferedReader reader = Files.newBufferedReader(specificationPath)) {
      ChangeSpecification changeSpecification = ChangeSpecificationParser
          .parseChangeSpecification(reader);
    int multiplicity = 1;

    OWLReasonerFactory reasonerFactory = new ReasonerFactory();

    assertTrue(ChangeCaseCreator.apply(ontology, ontologyPath.toString(), changeSpecification, reasonerFactory,
        this.tempDir, "test",
        new DistinctnessPolicy(multiplicity), 1, 0, null));
    }
  }

  @Test
  void inconsistentFromDisjointSuperTest()
      throws IOException, OWLOntologyCreationException, OWLOntologyStorageException {

    Path ontologyPath = Paths.get("src", "test", "resources", "alpha_full.owl.xml");
    OWLOntologyManager manager = OWLManager.createOWLOntologyManager();
    OWLOntology ontology = manager
        .loadOntologyFromOntologyDocument(Files.newInputStream(ontologyPath));
 
    Path specificationPath = Paths.get("src", "test", "resources", "inconsistentFromDisjointSuperConcepts.json");
 
    try (BufferedReader reader = Files.newBufferedReader(specificationPath)) {
      ChangeSpecification changeSpecification = ChangeSpecificationParser
          .parseChangeSpecification(reader);

      OWLReasonerFactory reasonerFactory = new JFactFactory();
      assertTrue(reasonerFactory.createReasoner(ontology).isConsistent());
       
      int multiplicity = 1;
      assertTrue(
          ChangeCaseCreator.apply(ontology, ontologyPath.toString(), changeSpecification, reasonerFactory,
              this.tempDir, "test",
              new DistinctnessPolicy(multiplicity), 9, 1, null));
    }
  }
}
